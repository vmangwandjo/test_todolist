<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\JourneeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/todos/historique', [TodoController::class, 'historique'])->name('todos.historique');

Route::get('/todos/journee', [JourneeController::class, 'journee'])
		->name('todos.journee');
Route::get('/todos/cloturer', [JourneeController::class, 'cloturer'])->name('todos.cloturer');

Route::get('/todos/achever', [TodoController::class, 'achever'])->name('todos.achever');
Route::get('/todos/done', [TodoController::class, 'done'])->name('todos.done');
Route::get('/todos/undone', [TodoController::class, 'undone'])->name('todos.undone');
Route::get('/todos/createbyme', [TodoController::class, 'createdbyme'])->name('todos.createdbyme');

Route::put('/todos/done/{todo}', [TodoController::class, 'makedone'])->name('todos.makedone');
Route::put('/todos/undone/{todo}', [TodoController::class, 'makeundone'])->name('todos.makeundone');

Route::resource("todos", TodoController::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
