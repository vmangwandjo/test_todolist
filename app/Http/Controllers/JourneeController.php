<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class JourneeController extends Controller
{

    public function journee()
    {
    	     //recover the current date
    $current_date = new \DateTime();
    $current_date = $current_date->format("Y-m-d");

    //recover the tomorrow's day
    $current_date_incr = new \DateTime();
    $current_date_incr->modify("+1 day");
    $current_date_incr = $current_date_incr->format("Y-m-d");
        $todos = Todo::where("created_at", ">=", $current_date)
                        ->where("created_at", "<", $current_date_incr)
                        ->paginate(20);
        return view("pages.journee", compact("todos"));
    }

    public function cloturer()
    {
    	//recover the current date
	    $current_date = new \DateTime();
	    $current_date = $current_date->format("Y-m-d");

	    //recover the tomorrow's day
	    $current_date_incr = new \DateTime();
	    $current_date_incr->modify("+1 day");
	    $current_date_incr = $current_date_incr->format("Y-m-d");

    	$todos = Todo::where("created_at", ">=", $current_date)
                        ->where("created_at", "<", $current_date_incr)
                        ->update(['done' => 1]);
    	notify()->success("Journée cloturée");

    	return redirect()->route('todos.index');
    }
}
