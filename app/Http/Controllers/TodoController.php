<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Todo;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RequestFromTodo;
use App\Http\Requests\RequestFromModal;
use Illuminate\Support\Carbon;

class TodoController extends Controller
{
    public $users;

    public function __construct()
    {
        $this->middleware('auth');
        $this->users = User::getAllUsers();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $todos = Todo::whereCreator_id($id)
                    ->orderBy("id", 'desc')
                    ->paginate(6);
        $users = $this->users;

        return view("pages.index", compact("todos", "users"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $todo = new Todo();
        return view("pages.create", compact("todo"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestFromTodo $request)
    {
        $todo = Todo::create([
            'name' => $request->name,
            "description" => $request->description,
            "creator_id" => Auth::user()->id,
            "date_fin" => $request->date_fin
        ]);

        if ($todo instanceof Todo) {
            notify()->success("Nouvelle tache #" . $todo->id . " a été ajouté avec success");

            return redirect()->route("todos.index");
        }
        
        notify()->error("An error occured while adding your todo. Please try agin");

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd("show");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        return view("pages.edit", compact('todo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  RequestFromTodo  $request, Todo $todo
     * @return \Illuminate\Http\Response
     */
    public function update(RequestFromTodo $request, Todo $todo)
    {
        if (!isset($request->done) OR $request->done == null) {
            $request['done'] = 0;
        }else{
            $request['done'] = 1;
        }
        $todo->update($request->all());

        notify()->info("Tache #" . $todo->id . " modifiée avec success");

        return redirect()->route('todos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        $todo->delete();
        notify()->error("Tache #" . $todo->id . " supprimée avec success !");
        
        return back();
    }

    /**
     * Change the statut of todo at undone.
     *
     * @param  Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function makeundone(Todo $todo)
    {
        $todo->done = 0;
        $todo->save();

        notify()->warning("Tache #" . $todo->id . " est ouverte de nouveau");

        return back();
    }

    /**
     * Change the statut of todo at done.
     *
     * @param  Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function makedone(Todo $todo)
    {
        $todo->done = 1;
        $todo->save();

        notify()->warning("Tache #" . $todo->id . " est terminée");

        return back();
    }

    /**
     * Give the todos undone.
     *
     * @return \Illuminate\Http\Response
     */
    public function undone()
    {
        $todos = Todo::whereCreator_idAndDone(Auth::user()->id, 0)->paginate(6);
        $users = $this->users;
        return view('pages.index', compact('todos', 'users'));
    }

    /**
     * Give the todos done.
     *
     * @return \Illuminate\Http\Response
     */
    public function done()
    {
        $todos = Todo::whereCreator_idAndDone(Auth::user()->id, 1)->paginate(6);
        $users = $this->users;
        return view('pages.index', compact('todos', 'users'));
    }
    
    /**
     * Give the todos who are passed.
     *
     * @return \Illuminate\Http\Response
     */
    public function achever()
    {
        $users = $this->users;

        //recover the current date
        $date = Carbon::now();
        $current_date = $date->toDateTimeString();

        $todos = Todo::where("date_fin", "<=", $current_date)
                        ->where("creator_id", Auth::user()->id)
                        ->paginate(6);
        return view("pages.index", compact("todos", "users"));
    }

    public function historique(Request $request)
    {
        $date = new \DateTime($request->date);
        $debut = new \DateTime($request->date);
        $debut = $debut->format("Y-m-d");
        $date->modify("+1 day");
        $date = $date->format("Y-m-d");
       
        $todos = Todo::whereBetween('created_at', [$debut, $date])
                        ->where("creator_id", Auth::user()->id)
                        ->paginate(14);
        $users = $this->users;

        return view("pages.index", compact("todos", "users", "debut"));
    }
}


