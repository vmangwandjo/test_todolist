<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Todo extends Model
{
    use HasFactory;
    use Notifiable;

    protected $fillable = [
        'name',
        'description',
        'creator_id',
        'done',
        "date_fin"
    ];

    public function user()
    {
    	return $this->belongsTo("App\Models\User", 'creator_id');
    }

}
