<?php

namespace Database\Factories;

use App\Models\Todo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TodoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Todo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(3),
            'creator_id' => $this->faker->numberBetween(1, 3),
            'done' => $this->faker->numberBetween(0,1),
            'affectedBy_id' => $this->faker->numberBetween(1, 3),
            'description' => $this->faker->sentence(10),
            'affectedTo_id' => $this->faker->numberBetween(1, 3),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
}
