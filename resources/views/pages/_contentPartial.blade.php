@csrf
<div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
	<label for="name">Nom: </label>
	<input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Enter the name's todo" value="{{old('name') ?: $todo->name}}" aria-describedby="nameHelp">
	<small class="text-muted form-text" id="nameHelp">Enter the name's todo</small>
	{!! $errors->first("name", "<span class='invalid-feedback' role='alert'>:message</span>") !!}
</div>
<div class="form-group">
	<label for="date_fin">Date de fin: </label>
	<input type="date" name="date_fin" id="date_fin" class="form-control @error('date_fin') is-invalid @enderror" placeholder="Enter the name's todo" value="{{old('date_fin') ?: $todo->date_fin}}" aria-describedby="nameHelp">
	{!! $errors->first("date_fin", "<span class='invalid-feedback' role='alert'>:message</span>") !!}
</div>
<div class="form-group">
	<label for="description">Description: </label>
	<textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" aria-describedby="nameHelp">{{old('description') ?: $todo->description}}</textarea>
	{!! $errors->first("description", "<span class='invalid-feedback'>:message</span>") !!}
</div>
<div class="form-group form-check">
	@if(Route::currentRouteName() == 'todos.edit')
	<input  class="form-check-input" type="checkbox" name="done" id="done" {{$todo->done==1 ? "checked" : ""}}>
	<label class="form-check-label" for="done">Fait ?</label>
	@endif
</div>
<button type="submit" class="btn btn-{{$class}}" name="created">{{$buttonName}}</button>
<a href="{{route('todos.index')}}" class="btn btn-default">Annuler</a>