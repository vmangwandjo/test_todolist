@extends("layouts.app")
@section("content")
	<div class="card">
		<div class="card-titlte card-header bg-success text-white">
			<h4>Creation de la tache</h4>
		</div>
		<div class="card-body">
			<form action="{{route('todos.store')}}" method="POST">
				@include("pages._contentPartial", [
					'class' => 'primary',
					'buttonName' => 'Creer la tache'
				])
			</form>
		</div>
	</div>
@endsection