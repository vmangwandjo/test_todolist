@extends("layouts.app")
@section("content")
{{--BUTTON --}}
@include("pages._button")

<div class="row form-inline justify-content-center my-3">
	<h1 class="mx-2 my-1">
		Aujourd'hui
		({{$todos->total()}})
	</h1>
	<a href="{{route('todos.cloturer')}}" class="btn btn-success mx-2 my-1">Cloturer la journee</a>
	<a href="{{route('todos.index')}}" class="btn btn-default">Acceuil</a>
</div>

@foreach($todos as $todo)
	@include("pages.todos.info")
@endforeach

{{$todos->links("vendor.pagination.bootstrap-4")}}
@endsection