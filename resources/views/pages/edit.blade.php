@extends("layouts.app")
@section("content")
	

	<div class="card">
		<div class="card-titlte card-header bg-success text-white">
			<h4>Modification de la tache <span class="badge badge-dark">#{{$todo->id}}</span></h4>
		</div>
		<div class="card-body">
			<form action="{{route('todos.update', $todo)}}" method="POST">
				@method("PUT")
				@include("pages._contentPartial", [
					'class' => 'success',
					'buttonName' => 'Modifier la tache'
				])
			</form>
		</div>
	</div>
@endsection