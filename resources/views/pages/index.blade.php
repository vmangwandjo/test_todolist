@extends("layouts.app")
@section("content")
{{--BUTTON --}}
@include("pages._button")

<h1>
	@if(Route::currentRouteName()=='todos.index')
	{{"Toutes les taches "}}
	@elseif(Route::currentRouteName()=='todos.achever')
	{{"Les taches achevées "}}
	@elseif(Route::currentRouteName()=='todos.done')
	{{"Les taches faites "}}
	@elseif(Route::currentRouteName()=='todos.undone')
	{{"Les taches en cours "}}
	@elseif(Route::currentRouteName()=='todos.historique')
	{{"Etat de la journee du "}}
	{{$debut}}
	@endif
	({{$todos->total()}})
</h1>

@foreach($todos as $todo)
	@include("pages.todos.info")
@endforeach

{{$todos->links("vendor.pagination.bootstrap-4")}}
@endsection