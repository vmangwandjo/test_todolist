<div class="row form-inline justify-content-center my-3">
	<a href="{{route('todos.create')}}" class="btn btn-primary mx-2 my-1">Ajouter une todo</a>
	@if(Route::currentRouteName()=='todos.index')
	<a href="" class="nav-link btn btn-dark mx-2 my-1" data-toggle="modal" data-target="#formulaire">Historique</a>
	<a href="{{route('todos.achever')}}" class="btn btn-info mx-2 my-1">Voir les taches achevées</a>
	<a href="{{route('todos.done')}}" class="btn btn-success mx-2 my-1">Voir les taches faites</a>
	<a href="{{route('todos.undone')}}" class="btn btn-warning mx-2 my-1">Voir les taches en cours</a>
	@elseif(Route::currentRouteName()=='todos.achever')
	<a href="" class="nav-link btn btn-dark mx-2 my-1" data-toggle="modal" data-target="#formulaire">Historique</a>
	<a href="{{route('todos.index')}}" class="btn btn-dark">Voir toutes les taches</a>
	<a href="{{route('todos.done')}}" class="btn btn-success mx-2 my-1">Voir les taches faites</a>
	<a href="/todos/undone" class="btn btn-warning mx-2 my-1">Voir les taches en cours</a>
	@elseif(Route::currentRouteName()=='todos.done')
	<a href="" class="nav-link btn btn-dark mx-2 my-1" data-toggle="modal" data-target="#formulaire">Historique</a>
	<a href="{{route('todos.achever')}}" class="btn btn-info mx-2 my-1">Voir les taches achevées</a>
	<a href="{{route('todos.index')}}" class="btn btn-dark">Voir toutes les taches</a>
	<a href="/todos/undone" class="btn btn-warning mx-2 my-1">Voir les taches ouvertes</a>
	@elseif(Route::currentRouteName()=='todos.undone')
	<a href="" class="nav-link btn btn-dark mx-2 my-1" data-toggle="modal" data-target="#formulaire">Historique</a>
	<a href="{{route('todos.achever')}}" class="btn btn-info mx-2 my-1">Voir les taches achevées</a>
	<a href="{{route('todos.index')}}" class="btn btn-dark">Voir toutes les taches</a>
	<a href="{{route('todos.done')}}" class="btn btn-success mx-2 my-1">Voir les taches faites</a>
	@elseif(Route::currentRouteName()=='todos.historique')
	<a href="{{route('todos.index')}}" class="btn btn-dark">Voir toutes les taches</a>
	<a href="{{route('todos.achever')}}" class="btn btn-info mx-2 my-1">Voir les taches achevées</a>
	<a href="/todos/undone" class="btn btn-warning mx-2 my-1">Voir les taches ouvertes</a>
	<a href="{{route('todos.done')}}" class="btn btn-success mx-2 my-1">Voir les taches faites</a>
	@endif
	
</div>