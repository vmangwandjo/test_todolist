<div class="alert alert-{{$todo->done==1 ? 'success' : 'warning'}}" role="alert">
		Crée le: {{$todo->created_at}}
		<br>
		Doit se terminer le: {{$todo->date_fin}}
		<details>
			<summary>
				<strong>{{$todo->name}}</strong>
				@if($todo->done==1)
				<span class="badge badge-primary">Done</span>
				@endif
				<br> {{$todo->description}}
			</summary>

			@if($todo->done==1)
			<br>
			<p>
				Terminée il y'a
		        {{ $todo->updated_at->from() }} - Terminée en
		        {{ $todo->updated_at->diffForHumans($todo->created_at, 1) }}
			</p>
			@endif
		</details>
		{{--button todos--}}
		<div class="form-inline justify-content-end my-1 p-0 col-sm">
			@if($todo->done==0)
			<form action="{{route('todos.makedone', $todo)}}" method="POST">
				@csrf
				@method("PUT")
				<button type="submit" class="btn btn-success mx-1 my-0" style="min-width:90px;">Fait</button>
			</form>
			@else
			<form action="{{route('todos.makeundone', $todo)}}" method="POST">
				@csrf
				@method("PUT")
				<button type="submit" class="btn btn-warning mx-1 my-0" style="min-width:90px;">En cours</button>
			</form>
			@endif
			<a class="btn btn-info mx-1 my-0" href="{{route('todos.edit', $todo)}}">Editer</a>
			<form action="{{route('todos.destroy', $todo)}}" method="POST" onsubmit="return confirm('Etes vous sur de vouloir supprimer ?');">
				@csrf
				@method("DELETE")
				<button type="submit" class="btn btn-danger mx-1 my-0">Supprimer</button>
			</form>
		</div>
	</div>